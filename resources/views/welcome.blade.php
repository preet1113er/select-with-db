<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">

        <!-- Styles -->
        <style>
            body {
                font-family: 'Nunito', sans-serif;
            }
        </style>
    </head>
    <body class="">
    <div class="container">
        <h2>student Login Info</h2>     
        <table class="table border">
            <thead>
            <tr>
                <th>Sl</th>
                <th>Student id</th>
                <th>User Name</th>
                <th>Email</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data as $key=>$student_login)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$student_login->student_info_id}}</td>
                <td>{{$student_login->user_name}}</td>
                <td>{{$student_login->email}}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <div class="container">
        <h2>student Info</h2>      
        <table class="table border">
            <thead>
            <tr>
                <th>Sl</th>
                <th>Name</th>
                <th>Mobile</th>
                <th>Address</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($data as $key=>$student_info)
            <tr>
                <td>{{$key+1}}</td>
                <td>{{$student_info->name}}</td>
                <td>{{$student_info->mobile}}</td>
                <td>{{$student_info->address}}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.7.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    </body>
</html>
