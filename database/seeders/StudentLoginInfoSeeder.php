<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class StudentLoginInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('student_logins')->insert([
            [
                'student_info_id'=> 1,
                'user_name'=> 'Student_1',
                'email'=> 'student1@gmail.com',
                'password' => Hash::make('12345678')
            ],
            [
                'student_info_id'=> 2,
                'user_name'=> 'Student_2',
                'email'=> 'student2@gmail.com',
                'password' => Hash::make('12345678')
            ],
            [
                'student_info_id'=> 3,
                'user_name'=> 'Student_3',
                'email'=> 'student3@gmail.com',
                'password' => Hash::make('12345678')
            ]
        ]);
    }
}
