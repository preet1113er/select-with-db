<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class StudentInfoSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('student_infos')->insert([
            [
                'name'=> 'Student 1',
                'mobile'=> '9877073348',
                'address'=> 'demo address, lane 1'
            ],
            [
                'name'=> 'Student 2',
                'mobile'=> '9877053348',
                'address'=> 'demo address, lane 2'
            ],
            [
                'name'=> 'Student 3',
                'mobile'=> '9877063348',
                'address'=> 'demo address, lane 3'
            ]
        ]);
    }
}
