<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StudentLogin extends Model
{
    use HasFactory;

    public function studentInfo()
    {
        return $this->belongsTo(StudentInfo::class);
    }
    
    public static function getAll()
    {
        return Self::with('studentInfo')->get();
    }
}
