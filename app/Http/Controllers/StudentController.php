<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\StudentLogin;

class StudentController extends Controller
{
    public function StudentPrintRecords()
    {
        // $data = StudentLogin::getAll();
        // return view('welcome', compact('data'));

        $dbName = "school_db";
        $dbTables = DB::select("SHOW TABLES FROM $dbName");
        $infoTable = $dbTables[9]->Tables_in_school_db;
        $loginTable = $dbTables[10]->Tables_in_school_db;

        $data = DB::select("SELECT * FROM $infoTable LEFT JOIN $loginTable ON $infoTable.id = $loginTable.student_info_id");

        return view('welcome', compact('data'));
    }
}
